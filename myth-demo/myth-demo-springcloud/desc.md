1. 启动注册中心
  打开 /myth/myth-demo/myth-demo-springcloud/myth-demo-springcloud-eureka
  启动 EurekaServerApplication
  打开网址  http://localhost:8761/ 查看注册
  
2.执行SpringcloudOrderApplication、SpringCloudAccountApplication、SpringCloudInventoryApplication、中的main方法， 启动前确保Kafka服务已启动。
  myth-demo-motan、myth-demo-dubbo的配置与启动类似，这里不再赘述哈~~
  
  
3.访问order服务，http://localhost:8884/swagger-ui.html 这是订单下单入口， 现在可以体验Springcloud分布式事务啦。
  
4. mac上安装Kafka
   采用mac下的安装利器homebrew，在终端输入brew install kafka即可，homebrew会自动安装kafka的依赖zookeeper。
   使用brew安装后，kafka和zookeeper的配置文件路径如下，通常情况下我们也不需要做任何修改。
   /usr/local/etc/kafka/server.properties
   /usr/local/etc/kafka/zookeeper.properties
   
5.使用下面两个命令快速启动zookeeper和kafka：
  
  brew services start zookeeper
  brew services start kafka
